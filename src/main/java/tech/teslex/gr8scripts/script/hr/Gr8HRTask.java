package tech.teslex.gr8scripts.script.hr;

import cn.nukkit.plugin.Plugin;
import cn.nukkit.scheduler.AsyncTask;
import tech.teslex.gr8scripts.script.Gr8Finder;
import tech.teslex.gr8scripts.script.api.Gr8Script;

import java.io.IOException;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.Optional;

public class Gr8HRTask extends AsyncTask {

	private final Plugin plugin;

	private final WatchService watchService;

	private final Gr8HRType type;

	public Gr8HRTask(Plugin plugin, WatchService watchService, Gr8HRType type) {
		this.plugin = plugin;
		this.watchService = watchService;
		this.type = type;
	}

	@Override
	public void onRun() {

		WatchKey watchKey;

		try {
			while ((watchKey = watchService.take()) != null) {

				watchKey.pollEvents().stream().filter(e -> e.kind() != StandardWatchEventKinds.OVERFLOW).forEach(event -> {

					plugin.getLogger().warning(event.kind() + " : " + event.context());

					if (type == Gr8HRType.SERVER) {
						plugin.getServer().reload();
					} else if (type == Gr8HRType.SCRIPT &&
							event.kind() != StandardWatchEventKinds.ENTRY_DELETE &&
							event.context().toString().endsWith(".groovy")
					) {
						Gr8Finder gr8Finder = new Gr8Finder(plugin);
						Optional<Gr8Script> script = gr8Finder.findOne(event.context().toString());

						if (script.isPresent()) {
							try {
								script.get().execute();
							} catch (IOException e) {
								e.printStackTrace();
							}
						}
					}
				});

				watchKey.reset();
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}