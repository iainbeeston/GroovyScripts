package tech.teslex.gr8scripts.script.api;

import groovy.lang.Script;
import tech.teslex.gr8scripts.script.ScriptLogger;

import java.io.IOException;

public interface Gr8Script {

	default String getVersion() {
		return "undefined";
	}

	default String getDescription() {
		return "";
	}

	default String getUsageInfo() {
		return "";
	}

	default Boolean isAutostart() {
		return true;
	}

	default Boolean isExecuted() {
		return false;
	}

	default ScriptLogger getLogger() {
		return new ScriptLogger(this);
	}

	default Object execute() throws IOException {
		return execute(new String[]{});
	}

	String getName();

	Script getScript();

	Object execute(String[] args) throws IOException;
}
