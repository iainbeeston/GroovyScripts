package tech.teslex.gr8scripts.registrator;

import cn.nukkit.event.Event;
import cn.nukkit.event.EventHandler;
import cn.nukkit.event.Listener;
import cn.nukkit.plugin.EventExecutor;
import cn.nukkit.plugin.Plugin;
import cn.nukkit.utils.EventException;
import tech.teslex.gr8scripts.annotation.GroovyNukkitEventHandler;
import tech.teslex.gr8scripts.script.api.Gr8Script;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;

public class GroovyNukkitEventHandlerRegister {

	private final Plugin plugin;
	private Gr8Script script = null;

	public GroovyNukkitEventHandlerRegister(Gr8Script script, Plugin plugin) {
		this.script = script;
		this.plugin = plugin;
	}

	public GroovyNukkitEventHandlerRegister(Plugin plugin) {
		this.plugin = plugin;
	}

	@EventHandler
	public void init(Object o) {

		Arrays.stream(o.getClass().getDeclaredMethods()).filter(m -> m.isAnnotationPresent(GroovyNukkitEventHandler.class) && m.getParameterTypes().length == 1).forEach(method -> {
			GroovyNukkitEventHandler eventOn = method.getAnnotation(GroovyNukkitEventHandler.class);

			if (plugin.getConfig().getSection("log").getBoolean("on_register_event"))
				if (script != null)
					script.getLogger().notice("Registering event §e" + method.getParameterTypes()[0].getSimpleName());
				else
					plugin.getLogger().notice("[" + o.getClass().getName() + "] Registering event §e" + method.getParameterTypes()[0].getSimpleName());

			plugin.getServer().getPluginManager().registerEvent(
					(Class<? extends Event>) method.getParameterTypes()[0],
					new Listener() {
					}, eventOn.priority(),
					new CustomMethodEventExecutor(method, o),
					plugin,
					eventOn.ignoreCancelled()
			);
		});
	}
}

class CustomMethodEventExecutor implements EventExecutor {
	private final Method method;
	private final Object o;

	CustomMethodEventExecutor(Method method, Object o) {
		this.method = method;
		this.o = o;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void execute(Listener listener, Event event) throws EventException {
		try {
			Class<Event>[] params = (Class<Event>[]) method.getParameterTypes();
			for (Class<Event> param : params) {
				if (param.isAssignableFrom(event.getClass())) {
					method.invoke(o, event);
					break;
				}
			}
		} catch (InvocationTargetException ex) {
			throw new EventException(ex.getCause());
		} catch (ClassCastException ex) {
			// We are going to ignore ClassCastException because EntityDamageEvent can't be cast to EntityDamageByEntityEvent
		} catch (Throwable t) {
			throw new EventException(t);
		}
	}

	public Method getMethod() {
		return method;
	}
}