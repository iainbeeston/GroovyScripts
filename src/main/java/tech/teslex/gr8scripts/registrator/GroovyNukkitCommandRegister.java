package tech.teslex.gr8scripts.registrator;

import cn.nukkit.command.Command;
import cn.nukkit.command.CommandSender;
import cn.nukkit.plugin.Plugin;
import tech.teslex.gr8scripts.annotation.GroovyNukkitCommand;
import tech.teslex.gr8scripts.script.api.Gr8Script;

import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;

public class GroovyNukkitCommandRegister {

	private final Plugin plugin;
	private Gr8Script script = null;

	public GroovyNukkitCommandRegister(Gr8Script script, Plugin plugin) {
		this.script = script;
		this.plugin = plugin;
	}

	public GroovyNukkitCommandRegister(Plugin plugin) {
		this.plugin = plugin;
	}

	public void register(Object o) {
		Arrays.stream(o.getClass().getDeclaredMethods()).filter(m -> m.isAnnotationPresent(GroovyNukkitCommand.class)).forEach(method -> {
			GroovyNukkitCommand command = method.getAnnotation(GroovyNukkitCommand.class);

			Command command1 = new Command(command.command(), command.description(), command.usage(), command.aliases()) {
				@Override
				public boolean execute(CommandSender commandSender, String s, String[] strings) {

					for (String permission : command.permissions())
						this.setPermission(permission);

					if (!this.testPermission(commandSender))
						return true;

					try {
						if (method.getReturnType().equals(Boolean.TYPE)) {
							return (boolean) (method.invoke(o, commandSender, s, strings));
						} else {
							method.invoke(o, commandSender, s, strings);
							return false;
						}
					} catch (IllegalAccessException | InvocationTargetException e) {
						e.printStackTrace();
					}

					return false;
				}
			};

			if (plugin.getConfig().getSection("log").getBoolean("on_register_command"))
				if (script != null)
					script.getLogger().notice("Registering command §e/" + command.command());
				else
					plugin.getLogger().notice("[" + o.getClass().getName() + "] Registering command §e/" + command.command());

			plugin.getServer().getCommandMap().register(command.command(), command1);
		});
	}
}
