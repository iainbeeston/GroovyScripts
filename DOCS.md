# Documentation

> Info about Groovy Language: [**Official Website**](http://groovy-lang.org)

> [**Scripts Examples**](https://gitlab.com/PiSystem/GroovyScriptsExamples/)



## Scripts Meta

Properties:
- `autostart` - execute script on server bootstrap (**default** `false`)
- `version` - script version (**default** `undefined`)
- `description` - info about script (**default** ``)
- `usageInfo` - instruction how to use script (**default** ``)


Defining Meta in scripts:

```groovy
@Field // required
meta = [
        autostart: true,
        version: '1.0',
        description: 'echo script',
        usageInfo: '/exec echo [text to echo]'
]

server.broadcastMessage(args.toString())
```


## Default Variables

- `server` - Server instance
- `plugin` - object of GroovyScript plugin
- `log` - script logger
- `events` - events registerer (tech.teslex.gr8scripts.registrator.GroovyNukkitEventHandlerRegister)
- `commands` - commands registerer (tech.teslex.gr8scripts.registrator.GroovyNukkitCommandRegister)
- `args` - arguments


## Events
Handle events

First way - just like in a plugin
Second way:
```groovy
import cn.nukkit.event.player.PlayerJoinEvent

@Event
def join(PlayerJoinEvent event) {
	// 
}

// and then
events.register(this)
```


## Commands
Registering commands

First way - just like in a plugin
Second way:
```groovy
import cn.nukkit.command.CommandSender

@Command(command = 'somecommand')
def somecmd(CommandSender sender, String cmd, String[] args) {
	// 
}

//and then
commands.register(this)
```

## Configuration

- `enabled` - enable scripts system
- `path` - path to scripts folder
- `autoexecute` - execute scripts on server bootstrap
- `compile-static` - compile scripts statically [ref](http://docs.groovy-lang.org/latest/html/gapi/groovy/transform/CompileStatic.html)
	> WARNING: when this property is true **Default Variables does not works**



#### Imports `imports`
Auto import

- `packages` - list of packages to import
- `classes` - list of classes to import
- `class-aliases` - map of alias and class to import
- `static` - list of classes to static import

#### Dev `dev`
Developer mode

- `enable` - enable dev mode
- `hot-reload` - reload server after script updating
	* `enable` - enable hot reload
	* `events` - list of events types `[create, modify, delete]`
	* `reload` - reload server or script `(server, script)`